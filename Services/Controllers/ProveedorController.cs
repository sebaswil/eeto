﻿using Business;
using Data;
using Entities;
using Microsoft.AspNetCore.Mvc;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProveedorController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Proveedor> Get()
        {
            ContextDB context = new ContextDB();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            ProveedorBusiness proveedor = new ProveedorBusiness(unitOfWork);
            return proveedor.Obtener();
        }
        [HttpPost]
        public void Insertar(Proveedor movimientoEntity)
        {
            ContextDB context = new ContextDB();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            ProveedorBusiness proveedor = new ProveedorBusiness(unitOfWork);
            proveedor.Insertar(movimientoEntity);
            unitOfWork.SaveTransaction();
        }
        [HttpPut]
        public void Actualizar(Proveedor movimientoEntity)
        {
            ContextDB context = new ContextDB();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            ProveedorBusiness proveedor = new ProveedorBusiness(unitOfWork);
            proveedor.Actualizar(movimientoEntity);
            unitOfWork.SaveTransaction();
        }
        [HttpDelete]
        public void Eliminar(Proveedor movimientoEntity)
        {
            ContextDB context = new ContextDB();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            ProveedorBusiness proveedor = new ProveedorBusiness(unitOfWork);
            var idProveedor = proveedor.Obtener().FirstOrDefault().IdProveedor;
            proveedor.Eliminar(idProveedor);
            unitOfWork.SaveTransaction();
        }
    }
}
