﻿using Business;
using Data;
using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovimientoController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Movimientos> Get()
        {
            ContextDB context = new ContextDB();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            MovimientoBusiness movimiento = new MovimientoBusiness(unitOfWork);
            return movimiento.Obtener();
        }
        [HttpPost]
        public void Insertar(Movimientos movimientoEntity)
        {
            ContextDB context = new ContextDB();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            MovimientoBusiness movimiento = new MovimientoBusiness(unitOfWork);
            movimiento.Insertar(movimientoEntity);
            unitOfWork.SaveTransaction();
        }
    }
}
