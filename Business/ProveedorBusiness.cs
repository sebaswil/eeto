﻿namespace Business
{
    using Entities;
    using Repository;
    using System;
    using System.Collections.Generic;

    public class ProveedorBusiness : IProveedor
    {
        private IUnitOfWork _unit;
        public ProveedorBusiness(IUnitOfWork unit)
        {
            this._unit = unit;
        }
        public void Actualizar(Proveedor proveedor)
        {
            this._unit.GenericRepository<Proveedor>().Update(proveedor);
        }

        public void Eliminar(int idProveedor)
        {
            this._unit.GenericRepository<Proveedor>().Delete(idProveedor);
        }

        public void Insertar(Proveedor proveedor)
        {
            this._unit.GenericRepository<Proveedor>().Insert(proveedor);
        }

        public IEnumerable<Proveedor> Obtener()
        {
            var lstProveedor = this._unit.GenericRepository<Proveedor>().Get();
            return lstProveedor;
        }
    }
}
