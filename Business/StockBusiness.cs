﻿namespace Business
{
    using Entities;
    using Repository;
    using System.Collections.Generic;
    public class StockBusiness : IStock
    {
        private IUnitOfWork _unit;
        public StockBusiness(IUnitOfWork unit)
        {
            this._unit = unit;
        }
        public void Actualizar(Stock stock)
        {
            this._unit.GenericRepository<Stock>().Update(stock);
        }

        public void Insertar(Stock stock)
        {
            this._unit.GenericRepository<Stock>().Insert(stock);
        }

        public IEnumerable<Stock> Obtener()
        {
            var lstStock = this._unit.GenericRepository<Stock>().Get();
            return lstStock;
        }
    }
}
