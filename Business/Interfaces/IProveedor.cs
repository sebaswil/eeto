﻿using Entities;
using System.Collections.Generic;

namespace Business
{
    public interface IProveedor
    {
        void Insertar(Proveedor proveedor);
        void Eliminar(int idProveedor);
        void Actualizar(Proveedor proveedor);
        IEnumerable<Proveedor> Obtener();
    }
}
