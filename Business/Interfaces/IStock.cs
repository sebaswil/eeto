﻿using Entities;
using System.Collections.Generic;

namespace Business
{
    public interface IStock
    {
        void Insertar(Stock stock);
        void Actualizar(Stock stock);
        IEnumerable<Stock> Obtener();
    }
}
