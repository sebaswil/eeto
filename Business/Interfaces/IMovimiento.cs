﻿using Entities;
using System.Collections.Generic;

namespace Business
{
    public interface IMovimiento
    {
        void Insertar(Movimientos movimiento);
        IEnumerable<Movimientos> Obtener();
    }
}
