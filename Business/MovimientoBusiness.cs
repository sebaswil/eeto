﻿namespace Business
{
    using Entities;
    using Microsoft.Data.SqlClient;
    using Repository;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    public class MovimientoBusiness : IMovimiento
    {
        private IUnitOfWork _unit;
        public MovimientoBusiness(IUnitOfWork unit)
        {
            this._unit = unit;
        }

        public void Insertar(Movimientos movimiento)
        {
            this._unit.GenericRepository<Movimientos>().Insert(movimiento);
            var producto = this._unit.GenericRepository<Stock>().FindSingleBy(x => x.IdProducto == movimiento.IdProducto);
            producto.CantidadDisponible = producto.CantidadDisponible - movimiento.Cantidad;
            this._unit.GenericRepository<Stock>().Update(producto);
        }

        public IEnumerable<Movimientos> Obtener()
        {
            var lstMovimientos = this._unit.GenericRepository<Movimientos>().Get();
            return lstMovimientos;
        }

    }
}
