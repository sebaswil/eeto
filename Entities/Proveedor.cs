﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    public class Proveedor
    {
        [Key]
        public int IdProveedor { get; set; }
        public string Identificacion { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Telefono { get; set; }
        public bool Estado { get; set; }

    }
}
