﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    public class Movimientos
    {
        [Key]
        public int IdMovimientos { get; set; }
        public string NombreMovimiento { get; set; }
        public int IdProducto { get; set; }
        public int Cantidad { get; set; }
    }
}
