﻿using Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Data
{
    public class ContextDB : DbContext, IContextDB
    {
        private string _connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=QualityData;Data Source=DESKTOP-MR52PR7\MSSQLSERVER01";

        public ContextDB(DbContextOptions options) : base(options) { }
        public ContextDB() { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!String.IsNullOrEmpty(_connectionString))
                options.UseSqlServer(_connectionString);
            base.OnConfiguring(options);
        }

        public virtual DbSet<Movimientos> movimientos { get; set; }
        public virtual DbSet<Productos> productos { get; set; }
        public virtual DbSet<Proveedor> proveedor { get; set; }
        public virtual DbSet<Stock> stock { get; set; }
    }
}
